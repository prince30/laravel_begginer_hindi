<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::group(['middleware' => 'web',
// 'namespace' =>'Admin',
// 'as' =>'admin-',
// 'admin-panel' =>'admin-',

// ], function () {
//     Route::get('dashboard',[
//     'as' =>'dashboard',
//     'uses' =>'AdminController@dashboard',
    
//     ]);
// });

// Route::group(['middleware' => 'web'],
//  function () {
    Route::get('/','AdminController@dashboard');

    Route::get('/admin',['AdminController@login',
        'as'=>'admin-login'       
    ]);

    Route::post('/form-submit',[
        'uses'=>'AdminController@formSubmit',
        'as'=>'f.submit'
    ]);
// });


//Route::get('/admin/{number}','AdminController@index' );

// Route::get('/admin/{number}', function ($number) {
//     echo "Number is $number";
// })->where(['number'=>"[0-9]+"]);

// Route::group(['middleware' => 'web',
// 'namespace' =>'Admin',
// 'as' =>'admin-',
// 'admin-panel' =>'admin-',

// ], function () {
//     Route::get('dashboard',[
//     'as' =>'dashboard',
//     'uses' =>'AdminController@dashboard',
    
//     ]);
// });

// Route::group([
// 'prefix' =>'admin-panel',

// ], function () {
//     Route::get('dashboard',[
//     'as' =>'dashboard',
//     'uses' =>'AdminController@dashboard',
//     ]);
// });

// Route::get('/', function () {
//     echo route('dashboard');
// });

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::group([
//     'prefix' =>'admin-panel',
    
//     ], function () {
//         Route::group([
//             'prefix' =>'subscribers',
            
//             ], function () {
//                 Route::get('add', function () {
//                     echo "go to hell .net";
//                 })->name('add-sub');
//             });
//     });
    
//     Route::get('/', function () {
//         echo route('add-sub');
//     });

// Route::any('admin', function () {
   
// });

// Route::match(['put','patch'],'admin', function () {
   
// });

//Route::controller('admin','AdminController');

// Route::get('/', function () {
//     $data=['Rahul','Amin'];
//     // return view('test_data',compact('data'));
//     //return view('test_data')->withNames($data);
//    // return View::make ('test-data');
//    return view('test_data')->withData($data);

// });

// Route::get('/', function () {

//    return "<h1>Hello world</h1>";

// })->middleware('logger');

// Route::get('/',[
//     'uses' => 
//     function () {
//     return "<h1>Hello world !</h1>";
 
//  },
//  'middleware'=>['web',],
//  ]);

// Route::controller('admin-panel','AdminController');


