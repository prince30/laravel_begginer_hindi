<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TestRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'field_one' => 'required|alpha',
            'field_two' => 'required|alpha'
        ];
    }

    public function messages()
{
    return [
        'required' => 'Field: attribute  can not be null',
    ];
}
}
