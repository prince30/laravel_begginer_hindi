<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable=[];
    protected $guarded=[];
    //jodi table name and column name change korte chan
    protected $table='customer_list';
    protected $primaryKey='customer_id';

    public $timestams=false;


    public function customerProfile() {
        return $this->hasOne('App\CustomerProfile');
    }

}
