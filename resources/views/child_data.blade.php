
@extends('layout.master')
@section('content')
<!-- {{-- 1st way--}}
{!!Form::open(['url'=>'form-submit'])
!!}

{{-- 2nd way--}}
{!!Form::open(['sction'=>'AdminController@formSubmit'])
!!} -->

{{-- 3rd way--}}
{!!Form::open(['route'=>'f.submit']) !!}
{!!Form::text('field_one')!!}
{!!Form::text('field_two')!!}
{!!Form::submit('submit')!!}

{!!Form::close() !!}



<!-- @foreach($errors->all() as $error)
<h1>{{$error}}</h1>
@endforeach -->

<?php 
echo($errors->first('field_one',"<li class='error'>:message</li>"));
echo($errors->first('field_two',"<li class='error'>:message</li>"));
 ?>
@endsection




