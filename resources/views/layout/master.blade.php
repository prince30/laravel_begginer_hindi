<!DOCTYPE html>
<html>
<head>
<title>Laravel 5.2 tutorial</title>
@stack('css')
</head>
<body>

<div class="container">
<h1>Master layout</h1>
@yield('content')
</div>
@stack('js')
</body>
</html> 